<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    
    public function logout(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->api_token = "";
        $user->save();

        if ($user->api_token == "") return response()->json([], 200);
    }

    public function login(Request $request) 
    {
        $messages = [
            'phone.required' => 'Введите номер телефона.',
            'password.required' => 'Введите пароль.',
        ];

        $rules = [
            'phone' => ['required'],
            'password' => ['required']
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        //Error Response (ошибка валидации)
        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        //Success Response
        if (Auth::attempt($request->all())) {
            $api_token = Str::random(32);

            $user = User::find(Auth::user()->id);
            $user->api_token = $api_token;
            $user->save();

            return response()->json(['token' => $api_token], 200);
        }

        //Error Response (неверный логин или пароль)
        return response()->json(['login' => 'Неверный логин или пароль'], 404);
    }
}
