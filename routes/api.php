<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Регистрация, авторизация
Route::group(['namespace' => 'Auth'], function () {
    Route::post('/signup', 'RegisterController@create');
    Route::post('/login', 'LoginController@login');

    Route::get('/login', function() {
        return response()->json(['message' => 'Вы должны авторизоваться'], 403);
    })->name('login');
});

//Выход
Route::group(['middleware' => 'auth:api'], function () {
    //Выход
    Route::post('/logout', 'Auth\LoginController@logout');

    //Работа с фото
    Route::group(['prefix' => 'photo'], function () {
        Route::post('/', 'PhotoController@create');
        Route::patch('/{id}', 'PhotoController@update');
        Route::get('/', 'PhotoController@getAll');
        Route::get('/{id}', 'PhotoController@get');
        Route::delete('/{id}', 'PhotoController@delete');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@getAll');
        Route::post('/{id}/share', 'UserController@share');
    });
});
