<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title', 'path', 'owner_id'
    ];

    protected $hidden = ['path'];
    protected $appends = ['url'];

    public function users() {
        return $this->belongsToMany('App\User');
    }

    public function getUrlAttribute()
    {
        return env('APP_URL').'/'.$this->attributes['path'];
    }
}
