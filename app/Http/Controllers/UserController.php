<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Photo;

class UserController extends Controller
{
    public function getAll(Request $request)
    {
        $search = explode(' ', $request->search);

        $response = User::where('id', '!=', Auth::user()->id);

        for ($i=0; $i < count($search); $i++) { 
            $response = $response->where(function ($query) use($search, $i) {
                $query
                    ->orWhere('first_name', 'like', '%'.$search[$i].'%')
                    ->orWhere('surname', 'like', '%'.$search[$i].'%')
                    ->orWhere('phone', 'like', '%'.$search[$i].'%');
            });
        }


        return response()->json([$request->search, $response->get()], 200);
    }

    public function share(Request $request, $id) {

        foreach ($request->photos as $photo_id) {

            $photo = Photo::find($photo_id);

           //Erroe Response (Ошибка доступа)
            if ($photo->owner_id != Auth::user()->id) {
                return response()->json(['message' => 'Ошибка доступа. Некоторые из выбранных изображений не ваши.'], 403);
            }

            $user = User::find($id);
            //для избежания дублей используем syncWithoutDetaching
            $user->photos()->syncWithoutDetaching($request->photos);

            $existing_photos = $user->photos()->get()->transform(function($photo) {
                return $photo->id;
            });

            return response()->json(['existing_photos' => $existing_photos], 201);
        }
        

        $photos_id = $request->photos;
        return response()->json([$photos_id], 201);
    }
}
