<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'unique:users', 'size:11'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $request
     * @return $user_id
     */
    protected function create(Request $request)
    {
        Validator::extend('phone_size', function($attr, $value, $parameters) {
            return strlen($value) == 11;
        });
        
        $messages = [
            'string' => 'Введите строку.',
            'numeric' => 'Используйте только цифры.',
            'max' => 'Слишком длинное значение.',
            'first_name.required' => 'Введите имя.',
            'surname.required' => 'Введите фамилию.',
            'phone.required' => 'Введите номер телефона.',
            'phone.unique' => 'Пользователь с таким номером телефона уже зарегистрирован.',
            'phone.phone_size' => 'Телефон должен состоять из 11 цифр.',
            'password.required' => 'Введите пароль.',
        ];

        $rules = [
            'first_name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'numeric', 'unique:users', 'phone_size'],
            'password' => ['required']
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        //Error Response
        if ($validator->fails()) return response()->json($validator->messages(), 422);

        $user_id = User::create([
            'first_name' => $request->first_name,
            'surname' => $request->surname,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
        ])->id;

        //Success Response
        return response()->json(['id' => $user_id], 201);
    }
}
