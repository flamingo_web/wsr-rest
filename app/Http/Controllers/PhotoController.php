<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use App\Photo;

class PhotoController extends Controller
{
    public function delete($id) {
        $photo = Photo::find($id);

        //Error Response (Ошибка доступа)
        if (Auth::user()->id != $photo->owner_id) return response()->json([], 403);
        
        unlink(public_path($photo->path));
        $photo->users()->detach();
        $photo->delete();

        //Success Response
        return response()->json([], 204);
    }

    public function get($id) {
        $photo = Photo::find($id);
        $photo->users->transform(function ($user) {
            return $user->id;
        });
        return response()->json($photo, 200);
    }

    public function getAll() {
        $photos = Photo::with('users')->get();
        $photos->transform(function ($photo) {
            $photo->users->transform(function ($user) {
                return $user->id;
            });
            return $photo;
        });

        return response()->json($photos, 200);
    }

    public function update(Request $request, $id)
    {
        $photo = Photo::find($id);

        //Error Response (Ошибка доступа)
        if (Auth::user()->id != $photo->owner_id) return response()->json([], 403);

        Validator::extend('base64', function($attr, $value) {
            return strpos($value, 'base64,');
        });

        $messages = [
            'title.max' => 'Слишком длинное значение',
            'base64' => 'Изображение должно быть в формате base64',
            'photo.string' => 'Изображение должно быть в формате base64'
        ];

        $rules = [
            'title' => ['max:255'],
            'photo' => ['string', 'base64']
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        //Error Response (Ошибка валидации)
        if ($validator->fails()) return response()->json($validator->messages(), 422);
        

        if ($request->has('title') && $request->title != "") $photo->title = $request->title;

        if ($request->has('photo')) {

            $photo_data = explode(',', $request->photo);
            $photo_exp = '.'.str_replace(['data:image/', ';base64'], '', $photo_data[0]);
            $photo_path = 'photos/'.Str::random(40).'.png';

            //создание изображения png из base64 (неважно base64 с png или с jpg)
            imagepng(imagecreatefromstring(base64_decode($photo_data[1])), public_path($photo_path));
            //удаление старое изображения
            unlink(public_path($photo->path));
            //обновление адреса изображения в БД
            $photo->path = $photo_path;
        }

        $photo->save();

        //Success Response
        return response()->json([
            'id' => $photo->id,
            'title' => $photo->title,
            'url' => $photo->url
        ], 200);

    }

    public function create(Request $request)
    {
        $messages = [
            'max' => 'Слишком длинное значение',
            'photo.required' => 'Добавьте изображение.',
            'photo.mimes' => 'Загрузите изображение формата jpg, jpeg или png.',
        ];

        $rules = [
            'title' => ['max:255'],
            'photo' => ['required', 'mimes:jpg,jpeg,png']
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        //Error Response
        if ($validator->fails()) return response()->json($validator->messages(), 422);

        $title = ($request->has('title') && $request->title != "") ? $request->title : 'Untitled';
        $path = $request->photo->store('/photos', 'public');

        $photo = Photo::create([
            'title' => $title,
            'path' => $path,
            'owner_id' => Auth::user()->id
        ]);
        
        //Success Response
        return response()->json([
            'id' => $photo->id,
            'title' => $photo->title,
            'url' => $photo->url
        ], 201);
    }
}
